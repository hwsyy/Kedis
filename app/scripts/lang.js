let lang = {
	"en": {
		"链接": "Link",
		"新建": "New",
		"编辑": "Edit",
		"删除": "Delete",
		"导出": "Export",
		"导入": "Import",
		"键": "Key",
		"新建STRING": "New STRING key",
		"新建HASH": "New HASH key",
		"新建SET": "New SET key",
		"新建ZSET": "New ZSET key",
		"新建LIST": "New LIST key",
		"撤销": "Undo",
		"重做": "Redo",
		"剪切": "Cut",
		"粘贴": "Paste",
		"选择所有": "Select all",
		"查看": "View",
		"重新加载": "Reload",
		"强制重新加载": "Force reload",
		"打开调试器": "Toggle devtools",
		"窗口": "Window",
		"帮助": "Help",
		"了解更多": "Learn more",
		"开始听写": "Start speaking",
		"结束听写": "Stop speaking",
		"关闭": "Close",
		"最小化": "Minimize",
		"放大": "Zoom",
        "放置到前端": "Put front",
        "错误":"Error",
        "这个键已经存在于当前库中，本操作这将可能会覆盖原有的键，是否确定要添加？":"This key is already exists, are you sure continue?",
        "键的名称不能为空。":"Key's name can not be null.",
        "键的初始化值不能为空。":"Key's init value can not be null.",
        "键的初始化字段名不能为空。":"Key's init field name can not be null."
	},
	"zh": {

	},
	"zh-CN": {

	},
	"zh-TW": {

	},
	"ja": {

	},
	"ko": [

	]
}
